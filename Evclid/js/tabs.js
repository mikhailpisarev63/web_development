const tabsBtn = document.querySelectorAll('.how__link-style');
const tabsItem = document.querySelectorAll('.how__item');

tabsBtn.forEach(el => {
  el.addEventListener('click', (e) => {
    const path = e.currentTarget.dataset.path;

    tabsBtn.forEach(el => el.classList.remove('how__link-active'));
    e.currentTarget.classList.add('how__link-active');

    tabsItem.forEach(el => el.classList.remove('how__item-active'));
    document.querySelector(`[data-target="${path}"]`).classList.add('how__item-active');
  });
});

const burger = document.querySelector('.burger');
const menu = document.querySelector('.header__nav');
const close = document.querySelector('.nav__close');

burger.addEventListener('click', () => {
  document.body.style.overflow = 'hidden';
  menu.classList.add('header__nav--visible');
});

close.addEventListener('click', () => {
  document.body.style.overflow = 'initial';
  menu.classList.remove('header__nav--visible');
});


window.addEventListener('DOMContentLoaded', function () {
  document.querySelector('#search').addEventListener('click', function () {
    document.querySelector('#header__search').classList.add('is-active')
  })
})

window.addEventListener('DOMContentLoaded', function () {
  document.querySelector('#search__close').addEventListener('click', function () {
    document.querySelector('#header__search').classList.remove('is-active')
  })
})

$(function () {
  $('.faq__list').accordion();
});

// slider
const swiper = new Swiper('.project__container', {
  slidesPerView: 1,
  loop: true,
  pagination: {
    el: '.project__pagination',
    clickable: true,
  },
});

